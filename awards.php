<?php
/**
 * @brief		Awards API
 */

namespace IPS\awards\api;

/* To prevent PHP errors (extending class does not exist) revealing path */
if ( !\defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
	header( ( isset( $_SERVER['SERVER_PROTOCOL'] ) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0' ) . ' 403 Forbidden' );
	exit;
}

/**
 * @brief	Awards API
 */
class _awards extends \IPS\Api\Controller
{
	//таблица соответствия наград удаленных и местных
		public $awards_links = array(
		'2'=>'1', 
		'5'=>'2', 
		'7' => '3'); 	
		
	//награждальщик	
		public $awards_giver = 1;
	//метод OAuth, по которому объединены пользователи форума и сайта
		public $OAuth_method = 1; 
	/**
	 * GET /awards/awards
	 * Get basic information about the community.
	 *
	 * @return	array
	 * @apiresponse	string	communityName	The name of the community
	 * @apiresponse	string	communityUrl	The community URL
	 * @apiresponse	string	ipsVersion		The Invision Community version number
	 */

	
	 
	public function GETindex()
	{
		return new \IPS\Api\Response( 200, array(
			'communityName'	=> \IPS\Settings::i()->board_name,
			'communityUrl'	=> \IPS\Settings::i()->base_url,
			'ipsVersion'	=> \IPS\Application::load('core')->version
		) );
	}
	/**
	 * POST /awards/awards
	 * Add items to awards table and cache 
	 *
	 * @apiparam	array 	users		пользователи и их награды
	 * @return		array
	 */
	public function POSTindex()
	{
		
		try {
			$this->giver = \IPS\Member::load($this->awards_giver);
		}
		catch (\Exception $e){
			return new \IPS\Api\Response( 404, 'MEMBER EXCEPTION on LOAD GIVER '.$this->awards_giver.' : '.$e->getMessage() );
		}
		
		$users = json_decode(\IPS\Request::i()->users);
		$delete_exclude = array();
		foreach ($users as $user) {
			unset($award);
			$id = $this->_getuserid($user->id);
			if ($id){ 
				if (\count($user->awards)){
					foreach ($user->awards as $award){
						$award_id = $this->_getawardid($award);
						if ( $award_id ) {
		
							try {
								$_award = \IPS\awards\Awards::load( $award_id );
							}
							catch (\Exception $e){
								$_users['errors'][] = 'AWARD EXCEPTION on LOAD: '.$e->getMessage(). ' on USER ' . $id .'('.$user->id.') and AWARD ' . $award_id .'('.$award.')';
								continue;
							}
							
							try {
								$member = \IPS\Member::load($id);
							}
							catch (\Exception $e){
								$_users['errors'][] = 'MEMBER EXCEPTION on LOAD RECEIVER: '.$e->getMessage(). ' on USER ' . $id .'('.$user->id.') and AWARD ' . $award_id .'('.$award.')';
								continue;
							}
							
							$delete_exclude[$award_id][] = $id;			
					
										
							$reason = 'API request';
									
							try {
								$_award->awardTo( $member, false, $this->giver, $reason );
							}
							catch (\Exception $e){
								$_users['errors'][] = 'AWARDING EXCEPTION: '.$e->getMessage(). ' on USER ' . $id .'('.$user->id.') and AWARD ' . $award_id .'('.$award.')';
								continue;
							}
							$_users['added'][$id][] = $award_id;
						} else {
							$_users['errors'][] = 'UNKNOWN_AWARD_ID: '.$award. ' for USER ' . $id .'('.$user->id.')';
						}
						
					}
				}
			} else {
				$_users['errors'][] = 'UNKNOWN_USER_ID:'.$user->id;
			}
			
			
		}
		if (\is_array($delete_exclude) AND \count($delete_exclude)){
				//$_users['removedARAY'] = $delete_exclude;
				foreach ($delete_exclude as $a => $de){
					if (\is_array($de) AND \count($de)){
						try{
							//$_users['removedSQL'][] = (string) \IPS\DB::i()->Select('awarded_id', 'awards_awarded', array(array('awarded_award = ' . $a) ,\IPS\Db::i()->in('awarded_member', $de, true)));
							foreach (\IPS\DB::i()->Select('awarded_id', 'awards_awarded', array(array('awarded_award = ' . $a) ,\IPS\Db::i()->in('awarded_member', $de, true))) as $remove_id){
								
								try
								{
									$remove = \IPS\awards\Awarded::load( (int) $remove_id  );
									
								} catch ( \UnderflowException $e ){
									continue;
								}
								$remove->remove( 'API request', $this->giver );
								$_users['removed'][] = $remove_id;
								
							}
						}
						catch (\OutOfRangeException $e){}
						
					}
				}
			}
		
		return new \IPS\Api\Response( 200, json_encode($_users) );
		
	}
	
	
		/**
	 * DELETE /awards/awards/{id}
	 * Deletes a award from all members
	 *
	 * @apiclientonly
	 * @param		int		$id			ID Number
	 * @return		void
	 */
	public function DELETEitem( $id )
	{
		
		try {
			$this->giver = \IPS\Member::load($this->awards_giver);
		}
		catch (\Exception $e){
			return new \IPS\Api\Response( 404, 'MEMBER EXCEPTION on LOAD GIVER '.$this->awards_giver.' : '.$e->getMessage() );
		}
		
		$award_id = $this->_getawardid($id);
		
		if ( $award_id ) {
			foreach (\IPS\DB::i()->Select('awarded_id', 'awards_awarded', array(array('awarded_award = ' . $award_id))) as $remove_id)
				{
					try
					{
						$remove = \IPS\awards\Awarded::load( (int) $remove_id  );
									
					} catch ( \UnderflowException $e ){
						continue;
					}
				
					$remove->remove( 'API request', $this->giver );
					$_users['removed'][$remove_id] = $id.'('.$award_id.')';
									
				}
		} else {
			return new \IPS\Api\Response( 404, 'UNKNOWN_AWARD_ID: '.$id );
		}		
		return new \IPS\Api\Response( 200, json_encode($_users) );
	}



	private function _getuserid($id) {
		try {
			$return = \IPS\DB::i()->Select('token_member', 'core_login_links', array('token_identifier = ? AND token_login_method = ?', $id,  $this->OAuth_method))->first();
		}
		catch ( \UnderflowException $e ){
			return false;
		}
		return $return;
	}
	private function _getawardid($id) {
		
		return $this->awards_links[$id];
	}
	
	
}